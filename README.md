# Trace Log4Net bridge

This library helps to redirect Trace messages to Log4Net.


## Usage

You can register the Log4NetTraceListener like this:

```csharp
var log4NetTraceListener = new Log4NetTraceListener();
Trace.Listeners.Add(log4NetTraceListener);
```

It is possible to configure the Log4NetTraceListener by providing
the name of the default logger and the default log level.

```csharp
string loggerName = "MyLogger";
TraceEventType eventType = TraceEventType.Verbose;
Log4NetTraceListener(loggerName, eventType)
```


## Suggest a logger

You can suggest logger name by enclosing it in braces at the beginning of the message and write a colon after it.

For example:
```csharp
var logger = "MyLogger";
Trace.TraceInformation($"[{logger}]: My message.");
```

The `Log4NetTraceListener` uses the `^\[(.*)\]\: ?(.*)` pattern to to extract parts from the trace message.
The default logger name is `TraceLogger`.


## Limitations

### Levels

Not all Log4Net log level can be used while using trace messages.
The following table shows the mapping:


| Trace            | Log4Net  |
| ---------------- | -------- |
| TraceInformation |  Info    |
| TraceWarning     | Warning  |
| TraceError       | Error    |
| Write(Line)      | Default<sup>1</sup> |

1.) The default value can be one of the following:
Debug, Info, Warn, Error or Fatal.

Combining the default value and the Write(Line) function of Trace, more Log4Net levels can be reached.


### Write/WriteLine

The two functions has the same effect because
the line layout is configured in Log4Net.


### Fail

The Traces Fail method prints messages like:
`Fail: <Message>`.
The library maps this string as is to the default level.
