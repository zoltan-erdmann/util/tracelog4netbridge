// SPDX-License-Identifier: MIT

namespace imperfect.log.trace.log4j.bridge
{
	internal delegate void WriteLogMessageWriter(string? message);
}
