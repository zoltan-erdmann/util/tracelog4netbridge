// SPDX-License-Identifier: MIT

namespace imperfect.log.trace.log4j.bridge
{
	public enum LogLevel
	{
		Debug,
		Info,
		Warn,
		Error,
		Fatal
	}
}
