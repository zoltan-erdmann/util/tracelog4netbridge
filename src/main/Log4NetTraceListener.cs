// SPDX-License-Identifier: MIT

using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using log4net;

namespace imperfect.log.trace.log4j.bridge
{
	public class Log4NetTraceListener : TraceListener
	{
		private const string TracePattern = @"^\[(.*)\]\: ?(.*)";
		
		private readonly string defaultLoggerName;
		private readonly TraceEventType defaultTraceEventType;
		
		public Log4NetTraceListener(
			string defaultLoggerName = "TraceLogger",
			TraceEventType defaultTraceEventType = TraceEventType.Verbose)
		{
			this.defaultLoggerName = defaultLoggerName;
			this.defaultTraceEventType = defaultTraceEventType;
		}

		public override void Write(string? message)
			=> TraceEvent(null, null, defaultTraceEventType, -1, message);

		public override void WriteLine(string? message)
			=> TraceEvent(null, null, defaultTraceEventType, -1, message);

		public override void TraceEvent(
			TraceEventCache? eventCache,
			string? source,
			TraceEventType eventType,
			int id)
			=> TraceEvent(eventCache, source, eventType, id, eventCache?.Callstack);

		private WriteLogMessageWriter LogMessageWriter(ILog logger, TraceEventType eventType)
			=> eventType switch
			{
				TraceEventType.Verbose => logger.Debug,
				TraceEventType.Error => logger.Error,
				TraceEventType.Critical => logger.Fatal,
				TraceEventType.Information => logger.Info,
				TraceEventType.Warning => logger.Warn,
				_ => throw new ArgumentOutOfRangeException(
					nameof(eventType), $"Not expected value: {eventType}")
			};

		public override void TraceEvent(
			TraceEventCache? eventCache,
			string? source,
			TraceEventType eventType,
			int id,
			string? message)
		{
			var logger = GetLogger(message);
			var strippedMessage = GetStrippedMessage(message);
			var WriteLogMessage = LogMessageWriter(logger, eventType);
			WriteLogMessage(strippedMessage);
		}

		private ILog GetLogger(string? message)
			=> LogManager.GetLogger(GetLoggerName(message));

		private string GetLoggerName(string? message)
			=> (message == null) ? defaultLoggerName : GetRegexGroupOrDefault(1, message, defaultLoggerName);

		private string? GetStrippedMessage(string? message)
			=> (message == null) ? null : GetRegexGroupOrMessage(2, message);

		private string GetRegexGroupOrMessage(int groupId, string message)
			=> GetRegexGroupOrDefault(groupId, message, message);
		
		private string GetRegexGroupOrDefault(int groupId, string message, string defaultValue)
		{
			Regex regex = new Regex(TracePattern, RegexOptions.IgnoreCase);
			Match match = regex.Match(message);
			if (match.Success)
			{
				if (match.Groups.Count > groupId)
				{
					Group group = match.Groups[groupId];
					return group.Value;
				}
			}
			return defaultValue;
		}
	}
}
