// SPDX-License-Identifier: MIT

using imperfect.log.trace.log4j.bridge;
using log4net;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using NUnit.Framework;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Test
{
	internal delegate void TraceMessageWriter(string? message);

	public class Log4NetTraceListenerTest
	{
		private const string LogLineRegexPattern = @"(.*)  ?(.*) - (.*)";
		private const string LogLinePattern = "%-5level %logger - %message";

		[Test]
		[TestCase("MyLogger1", TraceEventType.Information, "Info", "Simple log message.")]
		[TestCase("MyLogger2", TraceEventType.Verbose, "Debug", "")]
		[TestCase("MyLogger3", TraceEventType.Warning, "Warn", null)]
		[TestCase("MyLogger4", TraceEventType.Error, "Error", "Simple log message.")]
		[TestCase("MyLogger5", TraceEventType.Critical, "Fatal", "Simple log message.")]
		public void LoggerAndDefaultLevelCanBeSet(string loggerName, TraceEventType eventType, string levelText, string message)
		{
			// Given
			using var memoryStream = new MemoryStream();
			SetupMemoryStreamLogger(memoryStream, LogLinePattern);

			var log4NetTraceListener = new Log4NetTraceListener(loggerName, eventType);
			Trace.Listeners.Clear();
			Trace.Listeners.Add(log4NetTraceListener);

			// When
			Trace.WriteLine(message);

			// Then
			memoryStream.Position = 0; // Start reading from the beginning.
			using var streamReader = new StreamReader(memoryStream, Encoding.UTF8);
			var line = streamReader.ReadLine();

			var writtenLoggerName = GetLoggerName(line);
			var writtenLevel = GetLevel(line);
			var writtenMessage = GetMessage(line);

			StringAssert.AreEqualIgnoringCase(loggerName, writtenLoggerName);
			StringAssert.AreEqualIgnoringCase(levelText, writtenLevel);
			var expectedMessage = (message == null) ? "" : message;
			StringAssert.AreEqualIgnoringCase(expectedMessage, writtenMessage);
		}

		[Test]
		[TestCase("MyLogger1", TraceEventType.Information, "Info", "Simple log message.")]
		[TestCase("MyLogger2", TraceEventType.Warning, "Warn", "")]
		[TestCase("MyLogger3", TraceEventType.Error, "Error", null)]
		public void TraceWithLevelGetsLogged(string loggerName, TraceEventType eventType, string levelText, string message)
		{
			// Given
			using var memoryStream = new MemoryStream();
			SetupMemoryStreamLogger(memoryStream, LogLinePattern);

			var log4NetTraceListener = new Log4NetTraceListener(loggerName, TraceEventType.Verbose);
			Trace.Listeners.Clear();
			Trace.Listeners.Add(log4NetTraceListener);

			// When
			TraceMessageWriter WriteTraceMessage = SelectTraceWriter(eventType); 
			WriteTraceMessage(message);

			// Then
			memoryStream.Position = 0; // Start reading from the beginning.
			using var streamReader = new StreamReader(memoryStream, Encoding.UTF8);
			var line = streamReader.ReadLine();

			var writtenLoggerName = GetLoggerName(line);
			var writtenLevel = GetLevel(line);
			var writtenMessage = GetMessage(line);

			StringAssert.AreEqualIgnoringCase(loggerName, writtenLoggerName);
			StringAssert.AreEqualIgnoringCase(levelText, writtenLevel);
			var expectedMessage = (message == null) ? "" : message;
			StringAssert.AreEqualIgnoringCase(expectedMessage, writtenMessage);
		}

		private TraceMessageWriter SelectTraceWriter(TraceEventType eventType)
			=> eventType switch
			{
				TraceEventType.Information => (m) => Trace.TraceInformation(m),
				TraceEventType.Warning => (m) => Trace.TraceWarning(m),
				TraceEventType.Error => (m) => Trace.TraceError(m),
				_ => throw new ArgumentOutOfRangeException(nameof(eventType), $"Not expected value: {eventType}")
			};

		private void SetupMemoryStreamLogger(Stream stream, string pattern)
		{
			LogManager.ResetConfiguration();
			Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
			hierarchy.Root.RemoveAllAppenders();

			var patternLayout = CreatePatternLayout(pattern);
			var memoryStreamAppender = CreateMemoryStreamAppender(patternLayout, stream);
			hierarchy.Root.AddAppender(memoryStreamAppender);
			hierarchy.Root.Level = Level.All;
			hierarchy.Configured = true;
		}

		private MemoryStreamAppender CreateMemoryStreamAppender(PatternLayout patternLayout, Stream memoryStream)
		{
			MemoryStreamAppender memoryAppender = new MemoryStreamAppender(memoryStream);
			memoryAppender.Layout = patternLayout;
			memoryAppender.ActivateOptions();
			return memoryAppender;
		}

		private PatternLayout CreatePatternLayout(string pattern)
		{
			PatternLayout patternLayout = new PatternLayout();
			patternLayout.ConversionPattern = pattern;
			patternLayout.ActivateOptions();
			return patternLayout;
		}

		private string? GetLevel(string? line)
			=> line == null ? null : GetRegexGroupOrDefault(1, line);

		private string? GetLoggerName(string? line)
			=> line == null ? null : GetRegexGroupOrDefault(2, line);

		private string? GetMessage(string? line)
			=> line == null ? null : GetRegexGroupOrDefault(3, line);

		private string GetRegexGroupOrDefault(int groupId, string line)
		{
			Regex regex = new Regex(LogLineRegexPattern, RegexOptions.IgnoreCase);
			Match match = regex.Match(line);
			if (match.Success)
			{
				if (match.Groups.Count > groupId)
				{
					Group group = match.Groups[groupId];
					return group.Value.Trim();
				}
			}
			throw new ArgumentOutOfRangeException(nameof(line), $"Not expected value: {line}");
		}
	}
}
