// SPDX-License-Identifier: MIT

using log4net.Appender;
using log4net.Core;
using System;
using System.IO;
using System.Text;

namespace imperfect.log.trace.log4j.bridge
{
	internal sealed class MemoryStreamAppender : AppenderSkeleton, IDisposable
	{
		private readonly StreamWriter writer;

		public MemoryStreamAppender(Stream stream)
		{
			writer = new StreamWriter(stream, Encoding.UTF8);
			writer.AutoFlush = true;
		}

		public void Dispose()
		{
			writer?.Dispose();
		}

		protected override void Append(LoggingEvent loggingEvent)
		{
			writer.WriteLine(RenderLoggingEvent(loggingEvent));
		}
	}
}
